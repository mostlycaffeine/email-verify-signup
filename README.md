# Email Verify Signup Plugin
This WordPress plugin is a Work In Progress and shouldn't yet be used in production unless you know what you're doing. :)
The idea is that when a user registers they are sent via email a unique activation key URL, they then click the URL and assuming they are logged in then their email address will be verified. Currently there are no Admin Options but that is on its way a long with other features.

1. Install & Activate Plugin

2. A new page called "Verify email address" is automatically created.

3. That's basically it for now. You can see which users have successfully activated their email by going to Users and viewing the new column called "Email verified?"

## Notes
- This shouldn't yet be used in production and is a current WIP.
- Currently no options as it's a WIP.
- In the not too distent future Admin Options will be introduced along with content/account locking until email is activated.
- Hire me if you want me to adapt the code for your specific usage quicker, e.g. options / locking content until activated.

## Hire me
Hey! I'm a [freelance WordPress developer and technical consultant](https://mostlycaffeine.com) with a mission to deliver accessible, performant, secure websites that are sustainable and respect human rights. I'm available to hire for full projects or contract work, and would love to work with you!
- https://mostlycaffeine.com