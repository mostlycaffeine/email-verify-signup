<?php
/**
 * Email Verify Signup Plugin
 *
 *
 * @link              https://mostlycaffeine.com
 * @since             0.5.1
 * @package           emailverifysignup
 *
 * @wordpress-plugin
 * Plugin Name:       Email Verify Signup
 * Plugin URI:        https://mostlycaffeine.com/
 * Description:       Makes the user verify their email address before signing up. Probably best not to use in production yet.
 * Version:           0.5.1
 * Author:            @mostlycaffeine
 * Author URI:        https://mostlycaffeine.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       emailverifysignup
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {

	die;

}


/**
 * Current plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 * No ! define because it shouldn't be overwritten by anything else.
 */
define( 'EMAILVERIFYSIGNUP_VERSION', '0.5.1' );


if ( ! defined( 'EMAILVERIFYSIGNUP_PLUGIN_DIR' ) ) {

	define( 'EMAILVERIFYSIGNUP_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );

}


class MC_EmailVerifySignup {

	public function __construct() {

		// Plugin setup on activation.
		register_activation_hook( EMAILVERIFYSIGNUP_PLUGIN_DIR . 'email-verify-signup-pls.php', array( $this, 'mc_emailverifysignup_activationpage' ) );

		// Set and send new registrations an activation key.
		add_action( 'user_register', array( $this, 'mc_emailverifysignup_activationkey' ), 10, 1 );

		// Check verify URL.
		add_filter( 'query_vars', array( $this, 'mc_emailverifysignup_query_vars' ) );
		add_action( 'template_redirect', array( $this, 'mc_emailverifysignup_verifyurl' ) );

		// Column in Users page.
		add_filter( 'manage_users_columns', array( $this, 'mc_emailverifysignup_adduserscolumn_create' ) );
		add_filter( 'manage_users_custom_column', array( $this, 'mc_emailverifysignup_adduserscolumn_details' ), 10, 3 );
		add_filter( 'manage_users_sortable_columns', array( $this, 'mc_emailverifysignup_adduserscolumn_sortable' ) );

	}

	/**
	 * Create and send the random activation key to the user.
	 * @param int $user_id User ID.
	 */
	public function mc_emailverifysignup_activationkey( $user_id ) {

		// Check a page was actually created to be able to use.
		if ( ! empty( get_option( 'mc_emailverifysignup_activationpage' ) ) ) {

			// Get the page that was created on plugin activation.
			$mc_emailverifysignup_activationpage = (int) get_option( 'mc_emailverifysignup_activationpage' );

			// Check the page exists and is published.
			if ( 'publish' === get_post_status( $mc_emailverifysignup_activationpage ) ) {

				$user_info_id = (int) $user_id;
				$user_info    = get_userdata( $user_info_id );

				// Create a random activation key to use.
				$activation_key = sanitize_text_field( md5( time() ) );

				// Create activation key URL to send.
				$activation_url = get_permalink( $mc_emailverifysignup_activationpage ) . '?activation_key=' . $activation_key;

				// Tie the random activation key to the user.
				update_user_meta( $user_id, '_mc_email_verified', $activation_key );

				// Create email to send user their activation key.
				$to      = $user_info->user_email;
				$subject = __( 'Hey! Please verify...', 'emailverifysignup' );
				$body    = __( 'Please verify your email address by visiting: ', 'emailverifysignup' ) . esc_url( $activation_url );

				// Send the email.
				wp_mail( $to, $subject, $body, $headers );

			}
		}

	}

	/**
	 * Create a page for activation key to use if one hasn't already been added.
	 * This checks if one has been added already by checking the options for a
	 * page already set.
	 */
	public function mc_emailverifysignup_activationpage() {

		if ( ! get_option( 'mc_emailverifysignup_activationpage' ) ) {

			$activation_page = array(
				'post_name'   => 'emailverifysignup',
				'post_title'  => __( 'Verify email address' ),
				'post_status' => 'publish',
				'post_type'   => 'page',
			);

			$activation_page_id = wp_insert_post( $activation_page );

			add_option( 'mc_emailverifysignup_activationpage', $activation_page_id );

		}

	}

	/**
	 * Add activation_key querystrying key variable.
	 */
	public function mc_emailverifysignup_query_vars( $vars ) {

		$vars[] = 'activation_key';
		return $vars;

	}

	/**
	 * Verify URL with email.
	 */
	public function mc_emailverifysignup_verifyurl() {

		$mc_emailverifysignup_activationpage = (int) get_option( 'mc_emailverifysignup_activationpage' );

		$user_info_id = (int) get_current_user_id();
		$postid       = get_queried_object_id();

		// Check user is logged in and current page is the activation key page.
		if ( '0' !== $user_info_id && $postid === $mc_emailverifysignup_activationpage ) {

			$activation_key_query     = sanitize_text_field( get_query_var( 'activation_key' ) );
			$user_info_activation_key = get_user_meta( $user_info_id, '_mc_email_verified' );

			// Check current user matches the current activation key.
			if ( $user_info_activation_key === $activation_key_query ) {

				// Delete the activation key of user if email & key match.
				delete_user_meta( $user_info_id, '_mc_email_verified', $activation_key );

				// Add success message to the top of the_content.
				add_filter( 'the_content', array( $this, 'mc_emailverifysignup_verifycompleted' ) );

			} else {

				// Display a failed message as the key is wrong or the user is not logged in.
				add_filter( 'the_content', array( $this, 'mc_emailverifysignup_verifyfailed' ) );

			}
		}

	}

	/**
	 * Display the success message.
	 * @param string $content Content of the current post.
	 */
	public function mc_emailverifysignup_verifycompleted( $content ) {

		$content = sprintf(
			'<div class="mc_emailverifysignup_verifycompleted"><p>' . __( 'Thank you for verifying your email address.', 'emailverifysignup' ) . '</p></div>',
			$content
		);

		return $content;

	}

	/**
	 * Display the failed message.
	 * @param string $content Content of the current post.
	 */
	public function mc_emailverifysignup_verifyfailed( $content ) {

		$content = sprintf(
			'<div class="mc_emailverifysignup_verifyfailed"><p>' . __( 'Please make sure you are logged in and have the correct activation key.', 'emailverifysignup' ) . '</p></div>',
			$content
		);

		return $content;

	}

	/**
	 * Create a new column for the Users table.
	 * @param array $columns Array of all user table columns {column ID} => {column Name}
	 */
	public function mc_emailverifysignup_adduserscolumn_create( $columns ) {

		$columns['mc_email_verified'] = 'Email verified?';

		return $columns;

	}

	/**
	 * Create a new column for the Users table.
	 * @param string $row_output text/HTML output of a table cell
	 * @param string $column_id_attr column ID
	 * @param int $user user ID (in fact - table row ID)
	 */
	public function mc_emailverifysignup_adduserscolumn_details( $row_output, $column_id_attr, $user ) {

		if ( 'mc_email_verified' === $column_id_attr ) {

			if ( ! empty( get_the_author_meta( '_mc_email_verified', $user ) ) ) {

				return __( 'No', 'emailverifysignup' );

			} else {

				return __( 'Yes', 'emailverifysignup' );

			}
		}

		return $row_output;

	}

	/**
	 * Make the column sortable.
	 * @param array $columns Array of all user sortable columns {column ID} => {orderby GET-param}
	 */
	public function mc_emailverifysignup_adduserscolumn_sortable( $columns ) {

		return wp_parse_args( array( 'email_verified' => 'mc_email_verified' ), $columns );

	}

}
$mc_emailverifysignup = new MC_EmailVerifySignup();
